// alert("YOU'RE IN!")

function addTwoNumbers(addOne, addTwo) {
	console.log("I've got a " + addOne +  " + I've got a " + addTwo + " UGGHH! ")
	console.log(addOne+addTwo);
}

addTwoNumbers(3, 4);

function subtractTwoNumbers(subtractOne, subtractTwo) {
	console.log("I've got a " + subtractOne + " - I've got a " + subtractTwo + " UGGHH!")
	console.log(subtractOne - subtractTwo);
}

subtractTwoNumbers(10, 6);

function multiplyTwoNumbers(multiplicand, multiplier) {
	console.log("I've got a " + multiplicand + " * I've got a " + multiplier + " UGGHH!")
	 return multiplicand*multiplier;
	 
}

multiplyTwoNumbers(6, 10);

function divideTwoNumbers(dividend, divisor) {
	console.log("I've got a " + dividend + " / I've got a " + divisor + " UGGHH!" )
	return dividend/divisor;
}

let product = multiplyTwoNumbers(5, 25);
console.log(product);

let quotient = divideTwoNumbers(120, 4);
console.log(quotient);


function areaOfCircle(pi, radius) {
	console.log("The area of a Circle having a radius of " + radius + " and a constant value pi of " + pi + " is");
	return pi*(radius**2);
}
const valueOfPi = 3.1416
let circleArea = areaOfCircle(valueOfPi, 4);
console.log(circleArea);

function averageOfFourNumbers(numOne, numTwo, numThree, numFour) {
	console.log("The average value of "+numOne+", "+numTwo+", "+numThree+" and "+numFour+" is ");
	return (numOne + numTwo + numThree + numFour)/4; 
}

let averageVar = averageOfFourNumbers(10, 20, 30, 40);
console.log(averageVar);

function percentageCalculator(yourScore, totalItems) {
	console.log("Is "+yourScore+"/"+totalItems+" a passing score?")
	let percentage = (yourScore/totalItems)*100;
	let isPassed;
	return percentage >= 75;
}

let passingScore = percentageCalculator(16, 20);
console.log(passingScore);